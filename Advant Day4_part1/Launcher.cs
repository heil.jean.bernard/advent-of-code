﻿// See https://aka.ms/new-console-template for more information
try
{

    using (StreamReader sr = new StreamReader("C:/Users/formation/Desktop/advent.txt"))
    {
        bool byr = false;
        bool iyr = false;
        bool eyr = false;
        bool hgt = false;
        bool hcl = false;
        bool ecl = false;
        bool pid = false;
        int pass = 0;
        string? line = "toto";
        while ((line = sr.ReadLine()) != null)
        {
            while (!(String.IsNullOrEmpty(line)))
            {
                if (line.Contains("byr")) byr = true;
                if (line.Contains("iyr")) iyr = true;
                if (line.Contains("eyr")) eyr = true;
                if (line.Contains("hgt")) hgt = true;
                if (line.Contains("hcl")) hcl = true;
                if (line.Contains("ecl")) ecl = true;
                if (line.Contains("pid")) pid = true;
                line = sr.ReadLine();
            }
            if (byr && iyr && eyr && hgt && hcl && hcl && ecl && pid) pass = pass + 1;
            byr = false;
            iyr = false;
            eyr = false;
            hgt = false;
            hcl = false;
            ecl = false;
            pid = false;
        }
        Console.WriteLine(pass);
    }
}

catch (Exception e)
{
    Console.WriteLine("The file could not be read:");
    Console.WriteLine(e.Message);
}

