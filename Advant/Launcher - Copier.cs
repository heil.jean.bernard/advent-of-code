﻿// See https://aka.ms/new-console-template for more information
try
{
    using (StreamReader sr = new StreamReader("C:/Users/formation/Desktop/advent.txt"))
    {
        bool byr = false;
        bool iyr = false;
        bool eyr = false;
        bool hgt = false;
        bool hcl = false;
        bool ecl = false;
        bool pid = false;
        int pass = 0;
        string? line = "toto";
        string testeyecl = "amb blu brn gry grn hzl oth";

        while ((line = sr.ReadLine()) != null)
        {
            string[]? tabline = line.Split(' ', ':');

            while (!(string.IsNullOrEmpty(line)))
            {
                tabline = line.Split(' ', ':');

                for (int i = 0; i < tabline.Length; i++)
                {
                    if (tabline.GetValue(i).Equals("byr") && testbetween((string)tabline.GetValue(i + 1), 1920, 2002)) byr = true;

                    if (tabline.GetValue(i).Equals("iyr") && testbetween((string)tabline.GetValue(i + 1), 2010, 2020)) iyr = true;

                    if (tabline.GetValue(i).Equals("eyr") && testbetween((string)tabline.GetValue(i + 1), 2020, 2030)) eyr = true;

                    if (tabline.GetValue(i).Equals("hcl")
                        && ((string)tabline.GetValue(i + 1)).StartsWith("#")
                        && ((string)tabline.GetValue(i + 1)).Length == 7
                        && testhcl((string)tabline.GetValue(i + 1))
                            ) hcl = true;

                    if (tabline.GetValue(i).Equals("hgt")
                        && (testcm(((string)tabline.GetValue(i + 1))) || testin(((string)tabline.GetValue(i + 1))))) hgt = true;

                    if (tabline.GetValue(i).Equals("ecl") && testeyecl.Contains((string)tabline.GetValue(i + 1)) && ((string)tabline.GetValue(i + 1)).Length == 3) ecl = true;

                    if (tabline.GetValue(i).Equals("pid") && int.TryParse((string)tabline.GetValue(i + 1), out int result) && ((string)tabline.GetValue(i + 1)).Length == 9) pid = true;
                }
                line = sr.ReadLine();
            }
            if (byr && iyr && eyr && hgt && hcl && hcl && ecl && pid) pass = pass + 1;
            byr = false;
            iyr = false;
            eyr = false;
            hgt = false;
            hcl = false;
            ecl = false;
            pid = false;
        }
        Console.WriteLine(pass);
    }
}
catch (Exception e)
{
    Console.WriteLine("The file could not be read:");
    Console.WriteLine(e.Message);
}

static bool testhcl(string s)
{

    bool result = false;
    string test = "ghijklmnopqrstuv";

    foreach (char c in s)
    {
        if (test.Contains(c))
        {
            result = false;
            break;
        }
        else result = true;
    }

    return result;
}

static bool testbetween(string s, int start, int end)
{
    if (int.Parse(s) <= end && int.Parse(s) >= start) return true;
    else return false;
}

static bool testcm(string s)
{
    if (s.Length == 5) if (s.EndsWith("cm") && testbetween(s.Substring(0, 3), 150, 193)) return true;
        else return false;
    else return false;
}

static bool testin(string s)
{
    if (s.Length == 4) if (s.EndsWith("in") && testbetween(s.Substring(0, 2), 59, 76)) return true;
        else return false;
    else return false;
}


